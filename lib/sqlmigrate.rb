require "sqlmigrate/version"

module Sqlmigrate
  class SqlMigrateRailtie < Rails::Railtie
    rake_tasks do
      load File.join(File.dirname(File.expand_path(__FILE__)), 'tasks/db.rake')
    end
  end
end
