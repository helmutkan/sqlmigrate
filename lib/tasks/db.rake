require 'active_record'

namespace :db do
  namespace :migrate do
    def output_sql_file
      File.join(Rails.configuration.paths['db'].existent.first, 'migrations.sql')
    end

    def override_active_record_execute_method
      ActiveRecord::Base.connection.class.class_eval do
        alias :old_execute :execute

        def execute(sql, name = nil)
          if (/^(create|alter|drop|insert|delete|update)/i.match sql)
            File.open(output_sql_file, 'a') { |f| f.puts "#{sql};\n" }
          end
          old_execute sql, name
        end
      end
    end

    desc 'Generate plain SQL file from migrations'
    task sql: :environment do
      # ActiveRecord::Base.connection.initialize_schema_migrations_table
      # ActiveRecord::Base.connection.initialize_internal_metadata_table

      override_active_record_execute_method

      ActiveRecord::Base.logger = Logger.new(STDOUT)
      # Rake::Task['db:migrate'].invoke

      File.delete(output_sql_file) if File.exist?(output_sql_file)

      paths = ActiveRecord::Migrator.migrations_paths
      migrations = ActiveRecord::Migrator.migrations(paths)

      migrations.each do |migration_proxy|
        migration = migration_proxy.send(:load_migration)
        migration.migrate(:up)
      end
    end
  end
end
